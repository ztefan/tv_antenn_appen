package se.softstuff.min_tv_mast;

import junit.framework.TestCase;

import java.util.List;
import java.util.Locale;

import se.softstuff.min_tv_mast.teracom.TeracomTvTowerService;
import se.softstuff.min_tv_mast.teracom.Tower;

/**
 * Created by Stefan on 2013-11-30.
 */
public class TerracomTvTowerServiceTest extends TestCase {

    private TeracomTvTowerService instance;

    public void setUp() throws Exception {
        super.setUp();
        instance = new TeracomTvTowerService();
    }

    private void disabletestGetMyStations() {
        //curl "http://www.teracom.se/ajaxProxies/bestTransmitter.ashx?lat=59.360081400000006&lng=17.915179599999988" -H "Cookie: TS6e19e4=d77aebb050d12fe209e9b42e330991f785320f6ee435bff2529a33c4; __utma=82190355.197605561.1385837522.1385837522.1385837522.1; __utmb=82190355.2.10.1385837522; __utmc=82190355; __utmz=82190355.1385837522.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not"%"20provided)" -H "Accept-Encoding: gzip,deflate,sdch" -H "Host: www.teracom.se" -H "Accept-Language: sv,en-US;q=0.8,en;q=0.6" -H "User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36" -H "Content-Type: application/x-www-form-urlencoded" -H "Accept: application/json, text/javascript, */*" -H "Referer: http://www.teracom.se/mobil/rikta/" -H "X-Requested-With: XMLHttpRequest" -H "Connection: keep-alive" --compressed
        /*
        HTTP/1.1 200 OK
Date: Sat, 30 Nov 2013 18:51:59 GMT
X-Powered-By: ASP.NET
X-AspNet-Version: 2.0.50727
Cache-Control: private
Content-Type: application/json; charset=utf-8
Content-Length: 241

[{"StationID":"S","StationName":"Stockholm Nacka","SignalStrength":100,"CompassHeading":115,"DistanceInMetres":16172}
,{"StationID":"S/MBG","StationName":"Stockholm/Marieberg","SignalStrength":-1,"CompassHeading":122,"DistanceInMetres":6740}]

 String url = "http://www.teracom.se/ajaxProxies/bestTransmitter.ashx?lat=59.360081400000006&lng=17.915179599999988";
         */
        String expected = "[{\"StationID\":\"S\",\"StationName\":\"Stockholm Nacka\",\"SignalStrength\":100,\"CompassHeading\":115,\"DistanceInMetres\":16172}" +
                ",{\"StationID\":\"S/MBG\",\"StationName\":\"Stockholm/Marieberg\",\"SignalStrength\":-1,\"CompassHeading\":122,\"DistanceInMetres\":6740}]";

        double latitude = 59.360081400000006;
        double longitude = 17.915179599999988;
//        String response = instance.getMyStations(latitude, longitude);
//        assertEquals(expected, response);
    }

    public void testFormat() {

        double latitude = 59.360081400000006;
        double longitude = 17.915179599999988;

        String link = String.format(Locale.ENGLISH, "http://www.teracom.se/ajaxProxies/bestTransmitter.ashx?lat=%.4f&lng=%.4f", latitude, longitude);
        String expected = "http://www.teracom.se/ajaxProxies/bestTransmitter.ashx?lat=59.3601&lng=17.9152";
        assertEquals(expected, link);
    }

    public void testParseTowerJson() {

        String json = "[{\"StationID\":\"S\",\"StationName\":\"Stockholm Nacka\",\"SignalStrength\":100,\"CompassHeading\":115,\"DistanceInMetres\":16172}" +
                ",{\"StationID\":\"S/MBG\",\"StationName\":\"Stockholm/Marieberg\",\"SignalStrength\":-1,\"CompassHeading\":122,\"DistanceInMetres\":6740}]";
        List<Tower> towers = instance.parseTowerJson(json);
        assertEquals(2, towers.size());
        Tower nacka = towers.get(0);
        assertEquals("S", nacka.getStationID());
        assertEquals("Stockholm Nacka", nacka.getStationName());
        assertEquals(100, nacka.getSignalStrength());
        assertEquals(115, nacka.getCompassHeading());
        assertEquals(16172, nacka.getDistanceInMetres());

        Tower marieberg = towers.get(1);
        assertEquals("S/MBG", marieberg.getStationID());
        assertEquals("Stockholm/Marieberg", marieberg.getStationName());
        assertEquals(-1, marieberg.getSignalStrength());
        assertEquals(122, marieberg.getCompassHeading());
        assertEquals(6740, marieberg.getDistanceInMetres());
    }
}
