package se.softstuff.min_tv_mast.ui;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.common.base.Stopwatch;

import java.util.List;
import java.util.concurrent.TimeUnit;

import se.softstuff.min_tv_mast.teracom.Station;
import se.softstuff.min_tv_mast.storage.StationDao;


public class StationLoader implements GoogleMap.OnCameraChangeListener {

    private static final String TAG = "StationLoader";

    public interface OnStationReload{
        void visibleStationUpdate(List<Station> stations);
    }
    private final Context context;

    private final GoogleMap map;

    private float zoomLevel = 0;

    private final float detailLoadThreshold;

    private LatLngBounds loadedBounds;

    private OnStationReload onStationReload;

    private boolean wasLastLoadOverview;

    public StationLoader(final Context context, final GoogleMap googleMap, final float detailLoadThreshold) {
        this.context = context;
        this.map = googleMap;
        this.detailLoadThreshold = detailLoadThreshold;
        map.setOnCameraChangeListener(this);


    }

    public void setOnStationReload(OnStationReload onStationReload) {
        this.onStationReload = onStationReload;
        revalidate(map.getCameraPosition());
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        Log.d(TAG, String.format("onCameraChange %s zoom %f", cameraPosition.target, cameraPosition.zoom));
        revalidate(cameraPosition);
    }


    private void revalidate(CameraPosition cameraPosition) {

        if(zoomTriggerUpdate(cameraPosition) || pannedOutsideLoadedBound()){
            if(cameraPosition.zoom > detailLoadThreshold ){
                loadDetailedViewItems();
                wasLastLoadOverview = false;
            } else if(!wasLastLoadOverview) {
                loadOverviewItems();
                wasLastLoadOverview = true;
            }
            zoomLevel = cameraPosition.zoom;
        }
    }


    private void loadDetailedViewItems() {
        Stopwatch timer = new Stopwatch().start();
        LatLngBounds visible = map.getProjection().getVisibleRegion().latLngBounds;
        final int loadFactor = 1;
        final double horizontalSize = Math.max(1, visible.northeast.latitude - visible.southwest.latitude) ;
        final double verticalSize = Math.max(1,visible.northeast.longitude - visible.southwest.longitude) ;

        LatLng northeast = new LatLng(visible.northeast.latitude - verticalSize*loadFactor, visible.northeast.longitude+horizontalSize*loadFactor);
        LatLng southwest = new LatLng(visible.southwest.latitude + verticalSize*loadFactor, visible.southwest.longitude-horizontalSize*loadFactor);
        loadedBounds = new LatLngBounds(northeast, southwest);

        StationDao dao = new StationDao(context);
        List<Station> stations = dao.getAllStationIn(loadedBounds);

        refreshStations(stations);


        Log.d(TAG, String.format("loaded detailed view in %d ms loaded %d stations, horizontalSize:%f verticalSize:%f, ", timer.elapsed(TimeUnit.MILLISECONDS), stations.size(), horizontalSize, verticalSize));
    }



    private void loadOverviewItems() {
        Stopwatch timer = new Stopwatch().start();
        StationDao dao = new StationDao(context);
        List<Station> stations = dao.getAllLargeStation();

        refreshStations(stations);
        Log.d(TAG, String.format("loaded owerview in %d ms loaded %d stations", timer.elapsed(TimeUnit.MILLISECONDS), stations.size()));
    }

    private void refreshStations(List<Station> stations) {
        if(onStationReload != null){
            onStationReload.visibleStationUpdate(stations);
        }
    }

    private boolean pannedOutsideLoadedBound() {

        if(loadedBounds == null){
            return true;
        }
        final LatLngBounds visible = map.getProjection().getVisibleRegion().latLngBounds;
        return !(loadedBounds.contains(visible.northeast) && loadedBounds.contains(visible.southwest) );
    }

    private boolean zoomTriggerUpdate(CameraPosition cameraPosition) {

        if(zoomLevel < detailLoadThreshold){
            return cameraPosition.zoom > detailLoadThreshold;
        }
        return cameraPosition.zoom < detailLoadThreshold;
    }
}
