package se.softstuff.min_tv_mast.ui;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;

import se.softstuff.min_tv_mast.R;
import se.softstuff.min_tv_mast.teracom.Station;

/**
 * Created by Stefan on 2014-03-04.
 */
public class StationRenderer {

    public static MarkerOptions render(Station station) {
        if(station.isLarge()){
            return new MarkerOptions()
                    .title(station.getName())
                    .position(station.getPosition())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_tower_large))
                    .anchor(.4f,1f);
        } else {
            return new MarkerOptions()
                    .title(station.getName())
                    .position(station.getPosition())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_tower_small))
                    .anchor(.2f,1f);
        }
    }

    private StationRenderer(){}

}
