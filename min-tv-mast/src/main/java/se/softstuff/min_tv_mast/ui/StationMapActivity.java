package se.softstuff.min_tv_mast.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import se.softstuff.min_tv_mast.ui.view.Compass;
import se.softstuff.min_tv_mast.R;
import se.softstuff.min_tv_mast.teracom.Station;
import se.softstuff.min_tv_mast.ui.view.MapStation;
import se.softstuff.min_tv_mast.ui.view.StationEdge;
import se.softstuff.min_tv_mast.ui.view.StationRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public class StationMapActivity extends FragmentActivity  implements
        GoogleMap.OnMapLoadedCallback,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.InfoWindowAdapter,
        GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMapLongClickListener,
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener,
        LocationListener,
        StationLoader.OnStationReload,
        Compass.CompassListener, View.OnClickListener {


    public static final String TAG = "mymap";
    private static final String PREF_STATISTIC = "pref_stat";

    private GoogleMap mMap;
    private StationLoader loader;
    private StationRepository repository;

    private LatLng initCenter;
    private float initZoom;
    private int initMapType;

    private LocationClient locationClient;
    private Compass compass;
    private SensorManager mSensorManager;

    private EasyTracker easyTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_map);

        if(savedInstanceState != null){
            initCenter = savedInstanceState.getParcelable("initCenter");
            initZoom = savedInstanceState.getFloat("initZoom");
            initMapType = savedInstanceState.getInt("initMapType");
        }

        ((ImageButton)findViewById(R.id.myLocation)).setOnClickListener(this);
        ((ImageButton)findViewById(R.id.addPegman)).setOnClickListener(this);

        locationClient = new LocationClient(getApplicationContext(), this, this);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        compass = new Compass(mSensorManager);
        compass.setCompassListener(this);

        SharedPreferences sharedPrefs = getDefaultSharedPreferences(this);
        if (sharedPrefs.getBoolean(PREF_STATISTIC, true)) {
            easyTracker = EasyTracker.getInstance(this);
        }
    }

    /**
     * Save all appropriate fragment state.
     *
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if(mMap != null){
            final LatLng target = mMap.getCameraPosition().target;
            final float zoom = mMap.getCameraPosition().zoom;
            outState.putParcelable("initCenter", target);
            outState.putFloat("initZoom", zoom);
            outState.putInt("initMapType", mMap.getMapType());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.map_menu, menu);
        return true;
    }

    /**
     * Prepare the Screen's standard options menu to be displayed.  This is
     * called right before the menu is shown, every time it is shown.  You can
     * use this method to efficiently enable/disable items or otherwise
     * dynamically modify the contents.
     * <p/>
     * <p>The default implementation updates the system menu items based on the
     * activity's state.  Deriving classes should always call through to the
     * base class implementation.
     *
     * @param menu The options menu as last shown or first initialized by
     *             onCreateOptionsMenu().
     * @return You must return true for the menu to be displayed;
     * if you return false it will not be shown.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        if(mMap != null) {
            menu.findItem(R.id.maptype_normal).setChecked( mMap.getMapType() == GoogleMap.MAP_TYPE_NORMAL );
            menu.findItem(R.id.maptype_hybrid).setChecked( mMap.getMapType() == GoogleMap.MAP_TYPE_HYBRID );
            menu.findItem(R.id.maptype_satellite).setChecked( mMap.getMapType() == GoogleMap.MAP_TYPE_SATELLITE );
            menu.findItem(R.id.maptype_terrain).setChecked( mMap.getMapType() == GoogleMap.MAP_TYPE_TERRAIN );
        }
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (item.getItemId()){
            case R.id.action_compass :
                openCompassView(repository.getMe().getClosestStationEntities());
                return true;
            case R.id.maptype_normal :
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                return true;
            case R.id.maptype_hybrid :
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                return true;
            case R.id.maptype_satellite :
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                return true;
            case R.id.maptype_terrain :
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isEasyTrackerEnabled() {
        return easyTracker != null;
    }
    /*
     * Called when the Activity becomes visible.
     */
    @Override
    protected void onStart() {
        super.onStart();
        if (isEasyTrackerEnabled()) {
            easyTracker.activityStart(this);
        }
    }
    /*
     * Called when the Activity is no longer visible.
     */
    @Override
    protected void onStop() {
        if (locationClient.isConnected()) {
            locationClient.removeLocationUpdates(this);
        }
        locationClient.disconnect();

        if (isEasyTrackerEnabled()) {
            easyTracker.activityStop(this);
        }
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        compass.start();
        setUpMapIfNeeded();
        locationClient.connect();
    }

    @Override
    public void onPause() {
        compass.stop();
        super.onPause();
        if (locationClient.isConnected()) {
            locationClient.removeLocationUpdates(this);
        }
        locationClient.disconnect();
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.myLocation){
            flyToMyLocation();
        } else if(v.getId() == R.id.addPegman){
            repository.updatePegman(mMap.getCameraPosition().target,0);
            findViewById(R.id.addPegman).setVisibility(View.GONE);
        }
    }

    private void setUpMapIfNeeded() {
        if (mMap != null) {
            return;
        }
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMap = mapFragment.getMap();
        if (mMap == null) {
            return;
        }


        final int nrOfClosestStations = 7;
        repository = new StationRepository(this, mMap, nrOfClosestStations);


        final float detailLoadThreshold = 7f;
        loader = new StationLoader(getApplicationContext(), mMap, detailLoadThreshold);
        loader.setOnStationReload(this);

        mMap.setOnMarkerDragListener(this);
        mMap.setInfoWindowAdapter(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMapLongClickListener(this);

        setUpMapSettings();

        if(initCenter != null){
            moveTo(initCenter, initZoom);
        } else if(locationClient.isConnected() && locationClient.getLastLocation() != null) {
            flyToMyLocation();
        } else {

 //            new LatLng(59.330635,18.061885) ,7f)); //Stockholm
//            new LatLng((62.708897,17.035814), 4,53f); //Sweden
            final LatLng sweden = new LatLng(62.708897,17.035814);
            final float zoomLevel = 4.53f;
            moveTo(sweden, zoomLevel);
        }

        if(initMapType > 0) {
            mMap.setMapType(initMapType);
        }
    }



    private void setUpMapSettings() {
        UiSettings settings = mMap.getUiSettings();
        settings.setAllGesturesEnabled(false);
        settings.setCompassEnabled(false);
        settings.setRotateGesturesEnabled(false);
        settings.setScrollGesturesEnabled(true);
        settings.setZoomGesturesEnabled(true);
    }


    @Override
    public void onMapLoaded() {
        Log.d(TAG, "onMapLoaded");
        final Location myLocation = mMap.getMyLocation();
        if(myLocation != null) {
            repository.updatePegman(myLocation);
            repository.updateMyLocation(myLocation);
        }
    }

    private void flyToMyLocation(){
        final Location lastLocation = locationClient.getLastLocation();
        if(lastLocation != null){
            flyTo(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()), 10f);
            repository.updateMyLocation(lastLocation);
        }
        if(repository.isPegmanOnMap()){
            repository.deletePegman();
            findViewById(R.id.addPegman).setVisibility(View.VISIBLE);
        }
    }

    private void flyTo(Location target, float zoom) {
        LatLng latlng = new LatLng(target.getLatitude(), target.getLongitude());
        flyTo(latlng, zoom);
    }

    private void flyTo(LatLng target, float zoom) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(target, zoom));
    }

    private void moveTo(final LatLng moveTo, final float zoomLevel){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(moveTo, zoomLevel));
    }

    @Override
    public void visibleStationUpdate(List<Station> visibleStations) {
        repository.reload(visibleStations);
    }

    // OnMarkerDragListener
    @Override
    public void onMarkerDragStart(Marker marker) {
    }

    // OnMarkerDragListener
    @Override
    public void onMarkerDrag(Marker marker) {
        repository.updateStationRelationToPegman();
    }

    // OnMarkerDragListener
    @Override
    public void onMarkerDragEnd(Marker marker) {
        repository.updateStationRelationToPegman();
    }

    // InfoWindowAdapter
    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    // InfoWindowAdapter
    @Override
    public View getInfoContents(Marker marker) {
        if(repository.isPegman(marker)) {
            return createInfoContentForPegaman();
        } else if (repository.isMe(marker)) {
            return createInfoContentForMe();
        } else {
            final MapStation station = repository.findStationFor(marker);
            if(station == null){
                return null;
            }

            return createInfoContentForAStation(station);
        }
    }

    private View createInfoContentForAStation(MapStation station) {
        View view = getLayoutInflater().inflate(R.layout.info_window_station, null);
        assert view != null;

        ((TextView)view.findViewById(R.id.tvStationName)).setText(station.getName());
        ((TextView)view.findViewById(R.id.tvBearingFromMeValue)).setText(String.format("%d \u00B0",station.getBearing360()));
        ((TextView)view.findViewById(R.id.tvDistanceFromMeValue)).setText(String.format("%.1f km",station.getDistanceKm()));
        ((TextView)view.findViewById(R.id.tvHdReadyValue)).setText( station.isHdReady() ? R.string.yes : R.string.no );
        ((TextView)view.findViewById(R.id.tvComplementaryValue)).setText(station.isComplementary() ? R.string.yes : R.string.no);
        ((TextView)view.findViewById(R.id.tvLargeValue)).setText(station.isLarge() ? R.string.yes : R.string.no);

        return view;
    }

    private View createInfoContentForMe() {
        View view = getLayoutInflater().inflate(R.layout.info_window_me, null);
        assert view != null;
        LinearLayout llStations = (LinearLayout)view.findViewById(R.id.llStations);

        final List<MapStation> stations = repository.getMe().getClosestStations();
        for (MapStation stat : stations) {

            TextView valueTV = new TextView(this);
            valueTV.setText(getString(
                    R.string.info_view_me_station,
                    stat.isLarge() ? stat.getName()+"*" : stat.getName(),
                    Integer.valueOf(stat.getBearing360()),
                    Double.valueOf(stat.getDistanceKm())));
            valueTV.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            llStations.addView(valueTV);
        }

        return view;
    }

    private View createInfoContentForPegaman() {
        View view = getLayoutInflater().inflate(R.layout.info_window_me, null);
        assert view != null;

        TextView tvTitle = (TextView)view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.from_map_location);

        Button btnCompass = (Button)view.findViewById(R.id.btnCompass);
        btnCompass.setEnabled(false);
        btnCompass.setVisibility(View.GONE);

        LinearLayout llStations = (LinearLayout)view.findViewById(R.id.llStations);

        final List<MapStation> stations = repository.getPegman().getClosestStations();
        for (MapStation stat : stations) {
            TextView valueTV = new TextView(this);
            valueTV.setText(getString(
                    R.string.info_view_me_station,
                    stat.isLarge() ? stat.getName()+"*" : stat.getName(),
                    Integer.valueOf(stat.getBearing360()),
                    Double.valueOf(stat.getDistanceKm())));
            valueTV.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            llStations.addView(valueTV);
        }

        return view;
    }

    // OnInfoWindowClickListener
    @Override
    public void onInfoWindowClick(Marker marker) {

        if (repository.isMe(marker)) {
            openCompassView(repository.getMe().getClosestStationEntities());
        } else if(repository.isPegman(marker)){
            // ignore dont pop compass for pegman
        } else {
            final MapStation mapStation = repository.findStationFor(marker);
            openCompassView(Arrays.asList(mapStation.getStationEdge()));
        }
    }

    @Override
    public void onMapLongClick(LatLng position) {
        int accuracy = 0;
        repository.updatePegman(position, accuracy);
        findViewById(R.id.addPegman).setVisibility(View.GONE);
    }

    // GoogleMap.OnMarkerClickListener
    @Override
    public boolean onMarkerClick(Marker marker) {

        return false;
    }

//    GooglePlayServicesClient.ConnectionCallbacks,
//    GooglePlayServicesClient.OnConnectionFailedListener


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected bundle:"+bundle);
        LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(TimeUnit.SECONDS.toMillis(30))
                .setFastestInterval(TimeUnit.SECONDS.toMillis(5))
                .setSmallestDisplacement(100);

        locationClient.requestLocationUpdates(locationRequest, this);

        final Location myLocation = locationClient.getLastLocation();
        if (myLocation != null){
            updateLocation(myLocation);
        }
    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onLocationChanged(Location myLocation) {
        Log.d(TAG, "onLocationChanged "+myLocation);
        updateLocation(myLocation);
    }

    private void updateLocation(Location myLocation) {
        boolean firstFix = repository.getMe() == null;
        repository.updatePegman(myLocation);
        repository.updateMyLocation(myLocation);
        if(firstFix){
            flyTo(myLocation, 10f);
        }
    }


    // Compass.CompassListener
    @Override
    public void onCompassUpdate(SensorEvent event, long rate) {
        if(!compass.needCalibration() && repository.getMe() != null){
            repository.getMe().setRotation(compass.getHeading());
        }
    }


    private void openCompassView(Collection<StationEdge> targetEdges) {
        Intent intent = new Intent(this, CompassActivity.class);

        final ArrayList<StationEdge> arrayList = new ArrayList<StationEdge>(targetEdges);
        intent.putParcelableArrayListExtra(CompassActivity.TOWERS, arrayList);
        intent.setExtrasClassLoader(StationEdge.class.getClassLoader());
        startActivity(intent);
    }


}
