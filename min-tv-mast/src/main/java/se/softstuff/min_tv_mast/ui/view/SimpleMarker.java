package se.softstuff.min_tv_mast.ui.view;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import se.softstuff.min_tv_mast.teracom.Station;


public class SimpleMarker {


    public static final int HUNDRED_METER = 100;
    public static final float HIDDEN = 0.0f;
    public static final float VISIBLE = 1.0f;


    private final Marker marker;
    private int accuracy;

    public SimpleMarker(Marker marker, int accuracy) {
        this.marker = marker;
        this.accuracy = accuracy;
    }

    public void setRotation(float degrease) {
        marker.setRotation(degrease);

        if(degrease > 90 && degrease < 270){
            marker.setInfoWindowAnchor( .5f, 1.0f);
        } else {
            marker.setInfoWindowAnchor( .5f, 0.0f);
        }

    }

    public boolean isInfoWindowShown(){
        return marker.isInfoWindowShown();
    }

    public void updatePosition(Location myLocation) {
        final LatLng myNewPosition = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
        int accuracy = myLocation.hasAccuracy() ? (int)myLocation.getAccuracy() : -1;
        updatePosition(myNewPosition, accuracy);
    }
    public void updatePosition(LatLng position, final int accuracy) {
        marker.setPosition(position);
        this.accuracy = accuracy;
    }


    public boolean moveTo(Location currentPosition) {

        accuracy = currentPosition.hasAccuracy() ? (int)currentPosition.getAccuracy() : -1;
        final int moveMeters = distanceTo(currentPosition);
        if(moveMeters > HUNDRED_METER){
            final LatLng myNewPosition = new LatLng(currentPosition.getLatitude(), currentPosition.getLongitude());
            marker.setPosition(myNewPosition);
            return true;
        }
        return false;
    }

    public int distanceTo(Location position) {
        float[] results = new float[1];
        return distanceBetween(position, results);
    }

    public int distanceBetween(Location position, float[] results) {
        Location.distanceBetween(getLatitude(), getLongitude(), position.getLatitude(), position.getLongitude(), results);
        return (int)results[0];
    }

    public int distanceBetween(Station station, float[] results) {
        Location.distanceBetween(getLatitude(), getLongitude(), station.getLatitude(), station.getLongitude(), results);
        return (int)results[0];
    }

    public double getLatitude() {
        return marker.getPosition().latitude;
    }

    public double getLongitude() {
        return marker.getPosition().longitude;
    }

    public LatLng getPosition(){
        return marker.getPosition();
    }

    public boolean isSameMarker(Marker marker) {
        return this.marker.equals(marker);
    }


    public void showInfoWindow() {
        marker.showInfoWindow();
    }

    public void removeMarker() {
        marker.remove();
    }
}
