package se.softstuff.min_tv_mast.ui.view;

import android.app.Activity;
import android.location.Location;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import se.softstuff.min_tv_mast.R;
import se.softstuff.min_tv_mast.teracom.Station;


public class StationRepository {

    private MarkerWithStations me;
    private MarkerWithStations pegman;
    private Map<Station, MapStation> stations;
    private Map<Marker, MapStation> markerToStation;
    private final int nrOfClosestStations;

    private final Activity activity;
    private final GoogleMap map;

    public StationRepository(Activity activity, GoogleMap map, final int nrOfClosestStations){
        this.activity = activity;
        this.map = map;
        this.nrOfClosestStations = nrOfClosestStations;
        this.stations = new ConcurrentHashMap<Station, MapStation>();
        this.markerToStation = new ConcurrentHashMap<Marker, MapStation>();
    }

    public MarkerWithStations getMe() {
        return me;
    }

    public MarkerWithStations getPegman() {
        return pegman;
    }

    public void reload(final Collection<Station> stationList){

        final ImmutableList<MapStation> toRemove = FluentIterable.
                from(markerToStation.values())
                .filter(new Predicate<MapStation>() {
                    @Override
                    public boolean apply(MapStation input) {
                        return !stationList.contains(input.getStation());
                    }
                }).toList();

        for(MapStation old : toRemove){
            final Marker marker = old.getMarker();
            final MapStation removed = markerToStation.remove(marker);
            stations.remove(old.getStation());
            marker.remove();
        }

        for (Station station : stationList) {
            if(!stations.containsKey(station.getId())){
                final MapStation mapStation = MapStation.create(map, station);
                stations.put(station, mapStation);
                markerToStation.put(mapStation.getMarker(), mapStation);
            }
        }


//        List<Station> stationsToRemove = new ArrayList<Station>(stations.keySet());
//        stationsToRemove.removeAll(stationList);
//        for (Station toRemove : stationsToRemove) {
//            final MapStation mapStation = stations.remove(toRemove);
//            if(mapStation != null){
//                final Marker marker = mapStation.getMarker();
//                markerToStation.remove(marker);
//                marker.remove();
//            }
//        }
        if(isPegmanOnMap()){
            pegman.updateStationRelation(stations.values(), isInVisibleRegion(pegman.getPosition()));
        }

        if(me != null){
            me.updateStationRelation(stations.values(), isInVisibleRegion(me.getPosition()));
        }
    }

    private boolean isInVisibleRegion(LatLng position) {
        return map.getProjection().getVisibleRegion().latLngBounds.contains(position);
    }

    public void updateMyLocation(Location myLocation) {
        int accuracy = myLocation.hasAccuracy() ? (int)myLocation.getAccuracy() : -1;

        if(me == null ){
            me = MarkerFactory.createMe(map, myLocation, activity.getString(R.string.me), accuracy, 10);
            me.showInfoWindow();
        }
        me.updatePosition(myLocation);
        me.updateStationRelation(stations.values(), isInVisibleRegion(me.getPosition()));
    }

    public void updatePegman(Location location){
        if(!isPegmanOnMap()) {
            return; // ignore
        }
        final LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
        int accuracy = location.hasAccuracy() ? (int)location.getAccuracy() : -1;
        updatePegman(position, accuracy);
    }

    public void updatePegman(LatLng newPosition, int accuracy){
        if(!isPegmanOnMap()){
            pegman = MarkerFactory.createPegman(map, newPosition, activity.getString(R.string.pegman), accuracy, nrOfClosestStations);
            pegman.showInfoWindow();
        }

        pegman.updatePosition(newPosition, accuracy);
        updateStationRelationToPegman();
    }

    public void updateStationRelationToPegman(){
        pegman.updateStationRelation(
                stations.values(),
                isInVisibleRegion(pegman.getPosition()));
    }

    public MapStation findStationFor(Marker marker) {
         return markerToStation.get(marker);
     }

    public boolean isPegmanOnMap() {
        return pegman != null;
    }

    public boolean isPegman(Marker marker) {
        return isPegmanOnMap() && pegman.isSameMarker(marker);
    }

    public boolean isMe(Marker marker) {
        return me != null && me.isSameMarker(marker);
    }

    public void deletePegman() {
        if(isPegmanOnMap()){
            pegman.removeMarker();
            pegman = null;
        }
    }
}
