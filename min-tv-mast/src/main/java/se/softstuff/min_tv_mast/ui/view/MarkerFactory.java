package se.softstuff.min_tv_mast.ui.view;

import android.location.Location;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import se.softstuff.min_tv_mast.R;

/**
 * Created by Stefan on 2014-03-26.
 */
public class MarkerFactory {

    public static MarkerWithStations createMe(GoogleMap map, Location location, final String title, int accuracy, int nrOfClosestStations){
        final LatLng myPosition = new LatLng(location.getLatitude(), location.getLongitude());

        final Marker myMarker = map.addMarker(new MarkerOptions()
                .position(myPosition)
                .title(title)
                .draggable(false)
                .anchor(.5f, .5f)
//                .infoWindowAnchor(0.5f, 0.5f)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_arrow_red)));

        List<Polyline> lines = new ArrayList<Polyline>(nrOfClosestStations);
        for(int i = 0; i < nrOfClosestStations; i++){
            final Polyline polyline = map.addPolyline(new PolylineOptions().geodesic(false).width(3));
            lines.add(polyline);
        }
        return new MarkerWithStations(myMarker, accuracy, lines);
    }

    public static MarkerWithStations createPegman(GoogleMap map, LatLng myPosition, final String title, int accuracy, int nrOfClosestStations){
        final Marker myMarker = map.addMarker(new MarkerOptions()
                .position(myPosition)
                .title(title)
                .draggable(true)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
//                .anchor(.5f, .8f)
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pegman))
        );
        List<Polyline> lines = new ArrayList<Polyline>(nrOfClosestStations);
        for(int i = 0; i < nrOfClosestStations; i++){
            final Polyline polyline = map.addPolyline(new PolylineOptions().geodesic(false).width(3));
            lines.add(polyline);
        }
        return new MarkerWithStations(myMarker, accuracy, lines);
    }
}
