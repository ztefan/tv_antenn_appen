package se.softstuff.min_tv_mast.ui.view;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.common.primitives.Booleans;
import com.google.common.primitives.Ints;

import java.util.Comparator;

import se.softstuff.min_tv_mast.R;
import se.softstuff.min_tv_mast.teracom.Station;

/**
 * Created by Stefan on 2014-03-05.
 */
public class MapStation implements Comparable<MapStation> {

    public static final Comparator<MapStation> COMPARE_BY_DISTANCE = new Comparator<MapStation>() {
        @Override
        public int compare(MapStation lhs, MapStation rhs) {
            return Ints.compare(lhs.getDistance(), rhs.getDistance());
        }
    };

    public static final Comparator<MapStation> COMPARE_BY_SIZE_AND_DISTANCE = new Comparator<MapStation>() {
        @Override
        public int compare(MapStation lhs, MapStation rhs) {
            if(lhs.isLarge() == rhs.isLarge()) {
                return Ints.compare(lhs.getDistance(), rhs.getDistance());
            }
            return Booleans.compare(rhs.isLarge(), lhs.isLarge());
        }
    };


    public static MapStation create(GoogleMap map, Station station){
        final MarkerOptions markerOptions;
        if(station.isLarge()){
            markerOptions = new MarkerOptions()
                    .title(station.getName())
                    .position(station.getPosition())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_tower_large))
                    .anchor(.4f,1f);
        } else {
            markerOptions = new MarkerOptions()
                    .title(station.getName())
                    .position(station.getPosition())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_tower_small))
                    .anchor(.2f,1f);
        }

        final Marker marker = map.addMarker(markerOptions);

        return new MapStation(marker, station);
    }


    private final Marker marker;
    private StationEdge stationEdge;
    private Polyline line;

    public MapStation(Marker marker, Station station) {
        this.marker = marker;
        this.stationEdge = new StationEdge(station,0,0);
    }

    public Marker getMarker() {
        return marker;
    }

    public StationEdge getStationEdge() {
        return stationEdge;
    }

    public Station getStation() {
        return stationEdge.getStation();
    }


    public void setDistance(int distance) {
        stationEdge.setDistance(distance);
    }

    public float getDistanceKm() {
        return stationEdge.getDistanceKm();
    }

    public int getDistance() {
        return stationEdge.getDistance();
    }

    public int getBearing() {
        return stationEdge.getBearing();
    }

    public int getBearing360() {
        return stationEdge.getBearing360();
    }

    public void setBearing(int bearing) {
        stationEdge.setBearing(bearing);
    }

    /**
     * Compares this object to the specified object to determine their relative
     * order.
     *
     * @param another the object to compare to this instance.
     * @return a negative integer if this instance is less than {@code another};
     * a positive integer if this instance is greater than
     * {@code another}; 0 if this instance has the same order as
     * {@code another}.
     * @throws ClassCastException if {@code another} cannot be converted into something
     *                            comparable to {@code this} instance.
     */
    @Override
    public int compareTo(MapStation another) {
        return Ints.compare(getDistance(), another.getDistance());
    }

    public String getId() {
        return stationEdge.getStation().getId();
    }

    public String getName() {
        return stationEdge.getStation().getName();
    }

    public double getLatitude() {
        return stationEdge.getStation().getLatitude();
    }

    public double getLongitude() {
        return stationEdge.getStation().getLongitude();
    }

    public boolean isLarge() {
        return stationEdge.getStation().isLarge();
    }

    public boolean isComplementary() {
        return stationEdge.getStation().isComplementary();
    }

    public boolean isHdReady() {
        return stationEdge.getStation().isHdReady();
    }

    public boolean hasAlarm() {
        return stationEdge.getStation().hasAlarm();
    }

    public boolean hasNetMessage() {
        return stationEdge.getStation().hasNetMessage();
    }

    public LatLng getPosition() {
        return stationEdge.getStation().getPosition();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || ((Object) this).getClass() != o.getClass()) return false;

        MapStation that = (MapStation) o;

        if (!stationEdge.getStation().equals(that.stationEdge.getStation())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return stationEdge.getStation().hashCode();
    }




}
