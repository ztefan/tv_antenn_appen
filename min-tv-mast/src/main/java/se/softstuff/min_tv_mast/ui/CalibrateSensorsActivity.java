package se.softstuff.min_tv_mast.ui;

import android.app.Activity;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;

import se.softstuff.min_tv_mast.R;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public class CalibrateSensorsActivity extends Activity implements
        SensorEventListener {

    private static final String PREF_STATISTIC = "pref_stat";

    private SensorManager mSensorManager;
    private RatingBar ratingBarAcc;
    private RatingBar ratingBarMag;
    private TextView mMagValue;
    private TextView mMagWarnMsg;
    private TextView mCompassAxis;

    private float[] gravity = new float[3];
    private float[] geomagnetic = new float[3];
    private float[] matrixRotation = new float[9];
    private float[] matrixInclination = null;
    private float[] mOrientation = new float[3];

    private EasyTracker easyTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibrate_sensors);

        ratingBarAcc = (RatingBar) findViewById(R.id.ratingBarAcc);
        ratingBarMag = (RatingBar) findViewById(R.id.ratingBarMag);
        mMagValue = (TextView) findViewById(R.id.magValue);
        mMagWarnMsg = (TextView) findViewById(R.id.magWarnMsg);
        mCompassAxis = (TextView) findViewById(R.id.compassAxis);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        SharedPreferences sharedPrefs = getDefaultSharedPreferences(this);
        if (sharedPrefs.getBoolean(PREF_STATISTIC, true)) {
            easyTracker = EasyTracker.getInstance(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isEasyTrackerEnabled()) {
            easyTracker.activityStart(this);
        }
    }
    /*
     * Called when the Activity is no longer visible.
     */
    @Override
    protected void onStop() {
        if (isEasyTrackerEnabled()) {
            easyTracker.activityStop(this);
        }
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean supportsAcc = mSensorManager.registerListener(
                this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        boolean supportsMag = mSensorManager.registerListener(
                this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_NORMAL);

        if (!supportsAcc) {
            String txt = String.format("%s <b><font color=red>%s</font></br>", getString(R.string.acc_sensor), getString(R.string.missing));
            ((TextView) findViewById(R.id.titleAcc)).setText(Html.fromHtml(txt));
        }
        if (!supportsMag) {
            String txt = String.format("%s <b><font color=red>%s</font></br>", getString(R.string.mag_sensor), getString(R.string.missing));
            ((TextView) findViewById(R.id.titleAcc)).setText(Html.fromHtml(txt));
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            ratingBarAcc.setRating(event.accuracy);
        } else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            ratingBarMag.setRating(event.accuracy);
            double magneticFieldStrength = Math.sqrt((event.values[0] * event.values[0]) + (event.values[1] * event.values[1]) + (event.values[2] * event.values[2]));
            if (magneticFieldStrength >= SensorManager.MAGNETIC_FIELD_EARTH_MIN && magneticFieldStrength <= SensorManager.MAGNETIC_FIELD_EARTH_MAX) {
                String txt = getString(R.string.magnetic_field_sensor_value, magneticFieldStrength);
                mMagValue.setText(Html.fromHtml(txt));
                mMagWarnMsg.setVisibility(View.GONE);
            } else {
                String txt = getString(R.string.magnetic_field_sensor_value_warn, magneticFieldStrength);
                mMagValue.setText(Html.fromHtml(txt));
                mMagWarnMsg.setText(Html.fromHtml(getString(R.string.magnetic_field_sensor_value_warn_info)));
                mMagWarnMsg.setVisibility(View.VISIBLE);
            }
        }

        int type = event.sensor.getType();

        //Smoothing the sensor data a bit
        if (type == Sensor.TYPE_MAGNETIC_FIELD) {
            geomagnetic = lowPassFilter(event.values.clone(), geomagnetic);
        } else if (type == Sensor.TYPE_ACCELEROMETER) {
            gravity = lowPassFilter(event.values.clone(), gravity);
        }

        if ((type == Sensor.TYPE_MAGNETIC_FIELD) || (type == Sensor.TYPE_ACCELEROMETER)) {

            SensorManager.getRotationMatrix(matrixRotation, matrixInclination, gravity, geomagnetic);
            SensorManager.getOrientation(matrixRotation, mOrientation);

            final float azimuth = ((float) Math.toDegrees(mOrientation[0]) + 360) % 360;
            float pitch = (float) Math.toDegrees(mOrientation[1]);
            final float roll = (float) Math.toDegrees(mOrientation[2]);

            mCompassAxis.setText(getString(R.string.compass_axis, azimuth, pitch, roll));

        }


    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

        } else if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {

        }
    }

    private boolean isEasyTrackerEnabled() {
        return easyTracker != null;
    }

    private String getSensorAccuracyText(int accuracy) {
        switch (accuracy) {
            case SensorManager.SENSOR_STATUS_ACCURACY_HIGH:
                return getString(R.string.accuracy_high);
            case SensorManager.SENSOR_STATUS_ACCURACY_LOW:
                return getString(R.string.accuracy_low);
            default:
                return getString(R.string.accuracy_medium);
        }
    }

    protected float[] lowPassFilter(float[] input, float[] output) {
        if (output == null) return input;

        final float alpha = 0.10f;
        for (int i = 0; i < input.length; i++) {
            output[i] = output[i] + alpha * (input[i] - output[i]);
        }
        return output;
    }


    //http://developer.android.com/reference/android/hardware/SensorEvent.html
    protected float[] lowPassFilterGoogle(float[] input, float[] output) {

        if (output == null) return input;
        // alpha is calculated as t / (t + dT)
        // with t, the low-pass filter's time-constant
        // and dT, the event delivery rate

        final float alpha = 0.8f;
        for (int i = 0; i < input.length; i++) {
            output[i] = alpha * output[0] + (1 - alpha) * input[0];
        }

        return output;
    }
}
