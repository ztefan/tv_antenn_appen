package se.softstuff.min_tv_mast.ui.view;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.common.primitives.Booleans;
import com.google.common.primitives.Ints;

import java.util.Comparator;

import se.softstuff.min_tv_mast.teracom.Station;

public class StationEdge implements Comparable<StationEdge>, Parcelable {

    public static final Comparator<StationEdge> COMPARE_BY_DISTENCE = new Comparator<StationEdge>() {
        @Override
        public int compare(StationEdge lhs, StationEdge rhs) {
            return Ints.compare(lhs.getDistance(), rhs.getDistance());
        }
    };

    public static final Comparator<StationEdge> COMPARE_BY_SIZE_AND_DISTENCE = new Comparator<StationEdge>() {
        @Override
        public int compare(StationEdge lhs, StationEdge rhs) {
            if(lhs.isLarge() == rhs.isLarge()) {
                return Ints.compare(lhs.getDistance(), rhs.getDistance());
            }
            return Booleans.compare(rhs.isLarge(), lhs.isLarge());
        }
    };



    private final Station station;
    private int distance;
    private int bearing;

    public StationEdge(Station station, int distance, int bearing) {
        this.station = station;
        this.distance = distance;
        this.bearing = bearing;
    }

    public StationEdge(Station station, LatLng reference){
        this.station = station;
        float[] results = new float[2];
        Location.distanceBetween(reference.latitude, reference.longitude, station.getLatitude(), station.getLongitude(), results);
        distance = (int)results[0];
        bearing = (int)results[1];
    }

    public StationEdge(Parcel parcel) {
        station = parcel.readParcelable(Station.class.getClassLoader());
        this.distance = parcel.readInt();
        this.bearing = parcel.readInt();
    }

    public Station getStation() {
        return station;
    }

    public int getDistance() {
        return distance;
    }

    public float getDistanceKm() {
        return distance / 1000f;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getBearing() {
        return bearing;
    }


    public int getBearing360() {
        return (bearing+360)%360;
    }

    public void setBearing(int bearing) {
        this.bearing = bearing;
    }

    private boolean isLarge() {
        return station.isLarge();
    }

    public int compareTo(StationEdge another) {
        return Ints.compare(getDistance(), another.getDistance());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || ((Object)this).getClass() != o.getClass()) return false;

        StationEdge that = (StationEdge) o;

        if (!station.equals(that.station)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return station.hashCode();
    }

    @Override
    public String toString() {
        return String.format("%s%s, %d\u00B0 %2.0f km", station.getName(), isLarge()?"*":"", getBearing360(), getDistanceKm());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeParcelable(station, 0);
        out.writeInt(distance);
        out.writeInt(bearing);
    }

    public static final Parcelable.Creator<StationEdge> CREATOR
            = new Parcelable.Creator<StationEdge>() {
        public StationEdge createFromParcel(Parcel in) {
            return new StationEdge(in);
        }

        public StationEdge[] newArray(int size) {
            return new StationEdge[size];
        }
    };
}
