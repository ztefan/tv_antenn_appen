package se.softstuff.min_tv_mast.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import se.softstuff.min_tv_mast.services.TeracomUpdateService;
import se.softstuff.min_tv_mast.ui.view.Compass;
import se.softstuff.min_tv_mast.R;
import se.softstuff.min_tv_mast.teracom.Station;
import se.softstuff.min_tv_mast.storage.StationDao;
import se.softstuff.min_tv_mast.ui.view.StationEdge;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;
import static android.preference.PreferenceManager.setDefaultValues;


public class CompassActivity extends FragmentActivity implements
        GpsStatus.Listener,
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener,
        LocationListener,
        AdapterView.OnItemSelectedListener,
        Compass.CompassListener, View.OnClickListener {

    private static final String HEADING = "heading";

    private static final String LOG_TAG = "soft";
    private static final String PREF_STATISTIC = "pref_stat";
    private static final int RESULT_SETTINGS = 1;

    /*
     * Define a request code to send to Google Play services
     * This code is returned in Activity.onActivityResult
     */
    private final static int
            CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    static final String TOWERS = "towers";


    // define the display assembly compass picture
    private ImageView mImage;

    // record the compass picture angle turned
    private float currentDegree = 0f;
    private long lastVibrate;

    private LocationManager mLocationManager;
    private Vibrator mVibrator;
    private LocationClient locationClient;

    private View mCompassLayer;
    private TextView mTowerInfoTextView;
    private TextView mCompassTextView;
    private Compass mCompass;
    private ArrayList<StationEdge> mTowersRepository;
    private TextView msgWarn;
    private AlphaAnimation animBlink;

    private ArrayAdapter<StationEdge> mSpinnerArrayAdapter;
    private EasyTracker easyTracker;
    private StationEdge selectedTower;

    private TextView mAccuracy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compass);

        setDefaultValues(this, R.xml.preferences, false);

        locationClient = new LocationClient(this, this, this);

        mCompassLayer = findViewById(R.id.compassLayer);
        mImage = (ImageView) findViewById(R.id.ivAntennaItem);

        // TextView that will tell the user what degree is he heading
        mTowerInfoTextView = (TextView) findViewById(R.id.tvTowerInfo);
        mTowerInfoTextView.setText(R.string.tower_init);
        mCompassTextView = (TextView) findViewById(R.id.tvCompass);
        mAccuracy = (TextView)findViewById(R.id.tvAccuracy);
        mAccuracy.setText(getString(R.string.location_accuracy, "-"));

        Spinner towerSpinner = (Spinner) findViewById(R.id.towerSpinner);
        mTowersRepository = new ArrayList<StationEdge>();
        mSpinnerArrayAdapter = new ArrayAdapter<StationEdge>(this, android.R.layout.simple_spinner_dropdown_item, mTowersRepository);
        towerSpinner.setAdapter(mSpinnerArrayAdapter);
        towerSpinner.setOnItemSelectedListener(this);

        final Button map = (Button) findViewById(R.id.on_map);
        map.setOnClickListener(this);

        final Button calibrateGuide = (Button) findViewById(R.id.calibrateGuide);
        calibrateGuide.setOnClickListener(this);

        // initialize your android device sensor capabilities
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        mLocationManager.addGpsStatusListener(this);

        SensorManager mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mCompass = new Compass(mSensorManager);
        mCompass.setCompassListener(this);

        SharedPreferences sharedPrefs = getDefaultSharedPreferences(this);

        // 59.1876  17.6292  södertälje
        if (sharedPrefs.getBoolean(PREF_STATISTIC, true)) {
            easyTracker = EasyTracker.getInstance(this);
        }

        if (savedInstanceState != null) {
            mCompass.setHeading(savedInstanceState.getFloat(HEADING));
            updateCompassHeading(200);

            final ArrayList<StationEdge> selectedTowers = savedInstanceState.getParcelableArrayList(TOWERS);
            if (selectedTowers != null) {
                useStations(selectedTowers);
            }
        }

        if(getIntent().hasExtra(TOWERS)){
            getIntent().setExtrasClassLoader(StationEdge.class.getClassLoader());
            final ArrayList<StationEdge> selectedTowers = getIntent().getParcelableArrayListExtra(TOWERS);
            useStations(selectedTowers);
        }


        animBlink = new AlphaAnimation(0.0f, 1.0f);
        animBlink.setDuration(200); //You can manage the time of the blink with this parameter
        animBlink.setStartOffset(20);
        animBlink.setRepeatMode(Animation.REVERSE);
        animBlink.setRepeatCount(Animation.INFINITE);

        msgWarn = (TextView) findViewById(R.id.msgWarn);
        msgWarn.setVisibility(View.GONE);

        // check Google Play service APK is available and up to date.
        // see http://developer.android.com/google/play-services/setup.html
        final int result = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            Toast.makeText(this, "Google Play service is not available (status=" + result + ")", Toast.LENGTH_LONG).show();
            finish();
        }

        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showGPSDisabledAlertToUser();
        }

    }


    @Override
    public void onStart() {
        Log.d(LOG_TAG, "onStart");
        super.onStart();
        if (isEasyTrackerEnabled()) {
            easyTracker.activityStart(this);
        }

    }

    @Override
    public void onStop() {
        Log.d(LOG_TAG, "onStop");
        super.onStop();
        if (isEasyTrackerEnabled()) {
            easyTracker.activityStop(this);
        }
        if (locationClient.isConnected()) {
            locationClient.removeLocationUpdates(this);
        }
        locationClient.disconnect();
    }

    @Override
    protected void onResume() {
        Log.d(LOG_TAG, "onResume");
        super.onResume();


        mCompass.start();
        locationClient.connect();
    }

    @Override
    protected void onPause() {
        Log.d(LOG_TAG, "onPause");
        super.onPause();
        mCompass.stop();
        if (locationClient.isConnected()) {
            locationClient.removeLocationUpdates(this);
        }
        locationClient.disconnect();
    }

    @Override
    public void onBackPressed() {
        Log.d(LOG_TAG, "onBackPressed Called");
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putFloat(HEADING, mCompass.getHeading());
        outState.putParcelableArrayList(TOWERS, mTowersRepository);
    }


    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getString(R.string.msg_gps_disabled))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.enable_gps),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        }
                );
        alertDialogBuilder.setNegativeButton(getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        );
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private boolean isEasyTrackerEnabled() {
        return easyTracker != null;
    }

    private void trackTime(String event, long timeMs) {
        if (isEasyTrackerEnabled()) {
            Log.d(LOG_TAG, String.format("feed google analytic that %s took %s ms", event, timeMs));
            easyTracker.send(MapBuilder
                    .createTiming("resources",    // Timing category (required)
                            timeMs,       // Timing interval in milliseconds (required)
                            event,  // Timing name
                            null)           // Timing label
                    .build());
        } else {
            Log.d(LOG_TAG, String.format("do not feed google analytic that %s took %s ms", event, timeMs));
        }
    }


    @Override
    public void onCompassUpdate(SensorEvent event, long rate) {

        updateCompassHeading(rate);
    }


    private void updateCompassHeading(long rate) {

        if (mCompass.hasHeading() || selectedTower == null) {
            mCompassLayer.setVisibility(View.GONE);
            return;
        }

        mCompassLayer.setVisibility(View.VISIBLE);

        mCompassTextView.setText(getString(R.string.compass_heading, (int) mCompass.getHeading()));

        float toTarget = mCompass.getHeadingToTarget();

        if (mCompass.isUpsideDown()) {
            toTarget *= -1;
        }

        //mVibrator.hasVibrator level 11
        if (mVibrator != null && Math.abs(toTarget) < 5 && System.currentTimeMillis() - lastVibrate > 2000) {
            lastVibrate = System.currentTimeMillis();
            mVibrator.vibrate(100);
        }
        if (Math.abs(currentDegree - toTarget) > 1) {

            if (Math.abs(currentDegree - toTarget) < 350) {
                // create a rotation animation (reverse turn degree degrees)
                RotateAnimation ra = new RotateAnimation(
                        -currentDegree,
                        -toTarget,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f);

                // how long the animation will take place
                ra.setDuration(rate);
//                Log.d("", "rate "+rate);

                // set the animation after the end of the reservation status
                ra.setFillAfter(true);

//                Log.d(LOG_TAG, String.format("current %1$.1f to %2$.1f", currentDegree, toTarget));

                // Start the animation
                mImage.startAnimation(ra);
            }
            currentDegree = toTarget;
        }

        if (keepLevelMsgTTL < System.currentTimeMillis()) {
            if (mCompass.needCalibration()) {
                msgWarn.setText(R.string.need_to_calibrate);
                msgWarn.setVisibility(View.VISIBLE);
                msgWarn.startAnimation(animBlink);
                keepLevelMsgTTL = System.currentTimeMillis() + 2000;
            } else if (mCompass.hasMagneticInterference()) {
                msgWarn.setText(R.string.magnetic_interference);
                msgWarn.setVisibility(View.VISIBLE);
                msgWarn.startAnimation(animBlink);
                keepLevelMsgTTL = System.currentTimeMillis() + 2000;
            } else if (!mCompass.isKeeptLevel()) {
                msgWarn.setText(R.string.keep_level);
                msgWarn.setVisibility(View.VISIBLE);
                msgWarn.startAnimation(animBlink);
                keepLevelMsgTTL = System.currentTimeMillis() + 2000;
            } else if (msgWarn.getVisibility() == View.VISIBLE) {
                msgWarn.setVisibility(View.GONE);
                msgWarn.clearAnimation();
            }
        }

    }

    long keepLevelMsgTTL = 0;


    // list of towers was selected
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        selectedTower = mTowersRepository.get(pos);
        mCompass.setTargetHeading(selectedTower.getBearing360());
        String text = String.format(getString(R.string.tower_info), "<strong>" + selectedTower.getBearing360() + "</strong>");
        mTowerInfoTextView.setText(Html.fromHtml(text));

    }

    // list of towers was selected
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.compass_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
//            case R.id.menu_update_location:
//                loadLocationAndLoadTowers();
//                break;
//            case R.id.menu_settings:
//                Intent i = new Intent(this, SettingsActivity.class);
//                startActivityForResult(i, RESULT_SETTINGS);
//                break;

            case R.id.menu_map_view:
                openMapView();
                break;
            case R.id.menu_calibrate:
                openCalibrationView();
                break;
            case R.id.menu_rate:
                doRate();
                break;

            case R.id.menu_sync:
                syncDatabase();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void syncDatabase() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you like to syncronize the local database with the tower source?");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                TeracomUpdateService.startActionSyncronizeDatabase(CompassActivity.this);

            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_SETTINGS:
                SharedPreferences sharedPrefs = getDefaultSharedPreferences(this);
                if (sharedPrefs.getBoolean(PREF_STATISTIC, true)) {
                    easyTracker = EasyTracker.getInstance(this);
                } else {
                    easyTracker = null;
                }
                break;

            case CONNECTION_FAILURE_RESOLUTION_REQUEST:
            /*
             * If the result code is Activity.RESULT_OK, try
             * to connect again
             */
                switch (resultCode) {
                    case Activity.RESULT_OK:
                    /*
                     * Try the request again
                     */
                        locationClient.disconnect();
                        locationClient.connect();
                        break;
                }
        }

    }


    // GpsStatus.Listener
    @Override
    public void onGpsStatusChanged(int newStatus) {
//        if(newStatus != GpsStatus.GPS_EVENT_SATELLITE_STATUS) {
//            Log.d(LOG_TAG, "onGpsStatusChanged " + newStatus);
//        }

//        if(newStatus == GpsStatus.GPS_EVENT_STOPPED) {
//            Toast.makeText(this, "GPS has been stopped", Toast.LENGTH_LONG).show();
//        }
    }

    //    GooglePlayServicesClient.ConnectionCallbacks,
    @Override
    public void onConnected(Bundle bundle) {
        Log.d(LOG_TAG, "onConnected");
        LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(TimeUnit.SECONDS.toMillis(60))
                .setFastestInterval(TimeUnit.SECONDS.toMillis(5))
                .setSmallestDisplacement(500);
        locationClient.requestLocationUpdates(locationRequest, this);

        final Location lastLocation = locationClient.getLastLocation();
        if(lastLocation != null){
            if(mTowersRepository.isEmpty()) {
                reloadStations(lastLocation);
            } else {
                updateAccuracy(lastLocation);
            }
        }

        Log.d(LOG_TAG, String.format("lastLocation %s", lastLocation));

    }

    //    GooglePlayServicesClient.ConnectionCallbacks,
    @Override
    public void onDisconnected() {
        Log.d(LOG_TAG, String.format("onDisconnected"));
    }

    //    GooglePlayServicesClient.OnConnectionFailedListener,
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(LOG_TAG, String.format("onConnectionFailed %s", connectionResult));

        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                * Thrown if Google Play services canceled the original
                * PendingIntent
                */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
//            showErrorDialog(connectionResult.getErrorCode());
        }
    }


    //    LocationListener,
    @Override
    public void onLocationChanged(Location location) {
        Log.d(LOG_TAG, String.format("onLocationChanged %s", location));
        if(mTowersRepository.isEmpty()) {
            reloadStations(location);
        } else {
            updateAccuracy(location);
        }
    }

    private void reloadStations(final Location location) {

        new AsyncTask<Void, Void, List<StationEdge>>() {

            @Override
            protected List<StationEdge> doInBackground(Void... params) {
                StationDao dao = new StationDao(getApplicationContext());
                final List<Station> allStations = dao.getAllStation();
                final List<StationEdge> edges = new ArrayList<StationEdge>(allStations.size());
                final LatLng reference = new LatLng(location.getLatitude(), location.getLongitude());

                for (Station station : allStations) {
                    edges.add(new StationEdge(station, reference));
                }

                Collections.sort(edges, StationEdge.COMPARE_BY_DISTENCE);
                final int maxNrOfStations = 10;
                final ImmutableList<StationEdge> nearby = FluentIterable.from(edges).filter(new Predicate<StationEdge>() {
                    int addedCounter = 0;
                    boolean foundALarge = false;

                    @Override
                    public boolean apply(StationEdge input) {
                        if (++addedCounter > maxNrOfStations || foundALarge) {
                            return false;
                        }
                        foundALarge = input.getStation().isLarge();
                        return true;
                    }
                }).toSortedList(StationEdge.COMPARE_BY_SIZE_AND_DISTENCE);

                return nearby;
            }

            @Override
            protected void onPostExecute(List<StationEdge> stationEdges) {

                useStations(stationEdges);
                updateAccuracy(location);
            }
        }.execute();
    }

    private void updateAccuracy(Location location) {
        if(location.hasAccuracy()){
            mAccuracy.setText(getString(R.string.location_accuracy, (int)location.getAccuracy()));
        } else {
            mAccuracy.setText(getString(R.string.location_accuracy, "-"));
        }
    }

    private void useStations(List<StationEdge> stationEdges) {
        if (stationEdges != null) {
            mTowersRepository.clear();
            mTowersRepository.addAll(stationEdges);
            mSpinnerArrayAdapter.notifyDataSetChanged();
        } else {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.relacom_failed),
                    Toast.LENGTH_SHORT).show();
        }
    }


    private void openMapView() {
        Intent intent = new Intent(this, StationMapActivity.class);
        startActivity(intent);
    }

    private void openCalibrationView() {
        Intent cal = new Intent(this, CalibrateSensorsActivity.class);
        startActivity(cal);
    }

    private void doRate() {
    /* This code assumes you are inside an activity */
        final Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
        final Intent rateAppIntent = new Intent(Intent.ACTION_VIEW, uri);

        if (getPackageManager().queryIntentActivities(rateAppIntent, 0).size() > 0) {
            startActivity(rateAppIntent);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.rate_failed), Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.on_map){
            openMapView();
        } else if(v.getId() == R.id.calibrateGuide){
            openCalibrationView();
        }
    }
}
