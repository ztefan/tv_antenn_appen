package se.softstuff.min_tv_mast.ui.view;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.concurrent.TimeUnit;

/**
 * Created by Stefan on 2013-11-30.
 */
public class Compass implements SensorEventListener {

    public interface CompassListener {
        void onCompassUpdate(SensorEvent event, long rate);
//        void onDisturbance(boolean needCalibration);
    }
    private static final float ALPHA = 0.2f;
    private static final String TAG = "compass";

    private float[] gravity = new float[3];

    private float[] geomagnetic = new float[3];
    private float[] matrixRotation = new float[9];
    private float[] matrixInclination = null;
    private float[] mOrientation = new float[3];
    private float heading = -1;

    private float pitch = -1;
    private float roll = -1;
    private float targetHeading = 0;
    private boolean upsideDown;
    private long lastMagUpdate;

    private long rate = 200;
    // data for orientation values filtering (average using a ring buffer)
    static private final int RING_BUFFER_SIZE = 10;

    private float[][][] mAnglesRingBuffer;
    private int mNumAngles;
    private int mRingBufferIndex;
    private float[][] mAngles;
    private boolean magneticInterference;

    private final SensorManager sensorManager;

    private boolean needMagCalibration;
    private boolean needAccCalibration;

    private CompassListener compassListener;

    public Compass(SensorManager sensorManager) {
        this.sensorManager = sensorManager;
        // initialize the ring buffer for orientation values
        mNumAngles = 0;
        mRingBufferIndex = 0;
        mAnglesRingBuffer = new float[RING_BUFFER_SIZE][3][2];
        mAngles = new float[3][2];
        mAngles[0][0] = 0;
        mAngles[0][1] = 0;
        mAngles[1][0] = 0;
        mAngles[1][1] = 0;
        mAngles[2][0] = 0;
        mAngles[2][1] = 0;
    }

    public void setCompassListener(CompassListener compassListener) {
        this.compassListener = compassListener;
    }

    public void start() {
        sensorManager.registerListener(
                this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(
                this,
                sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_NORMAL);
        Log.d(TAG, "Compass started");
    }

    public void stop() {
        sensorManager.unregisterListener(this);
        Log.d(TAG, "Compass stopped");
    }


    public void onSensorChanged(SensorEvent event) {

        int type = event.sensor.getType();

        //Smoothing the sensor data a bit
        if (type == Sensor.TYPE_MAGNETIC_FIELD) {
            measureRate(event);
            geomagnetic = lowPassFilter(geomagnetic, event.values.clone(), rate);
            measureMagneticInterference(event);
            needMagCalibration = event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE;
//            Log.d("compass",String.format("MAGNETIC_FIELD  %1$.1f, %2$.1f, %3$.1f", geomagnetic[0], geomagnetic[1], geomagnetic[2] ));
        } else if (type == Sensor.TYPE_ACCELEROMETER) {
            gravity = lowPassFilter(gravity, event.values.clone(), rate);
//            Log.d("compass",String.format("ACCELEROMETER   %1$.1f, %2$.1f, %3$.1f", geomagnetic[0], geomagnetic[1], geomagnetic[2] ));
            needAccCalibration = event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE;
        }

        if ((type == Sensor.TYPE_MAGNETIC_FIELD) || (type == Sensor.TYPE_ACCELEROMETER)) {

            SensorManager.getRotationMatrix(matrixRotation, matrixInclination, gravity, geomagnetic);
            SensorManager.getOrientation(matrixRotation, mOrientation);

            final float azimuth = (float) Math.toDegrees(mOrientation[0]);
            float pitch = (float) Math.toDegrees(mOrientation[1]);
            final float roll = (float) Math.toDegrees(mOrientation[2]);


            measureIfUpsideDown(roll);


//          Adjust the range: 0 < range <= 360 (from: -180 < range <= 180).
            this.heading = (azimuth + 360) % 360;
            this.pitch = pitch;
            this.roll = roll;

//            Log.d("compass",String.format("heading: %1$.1f pitch: %2$.1f roll: %3$.1f", this.heading, this.pitch, this.roll));


            if (compassListener != null) {
                compassListener.onCompassUpdate(event, rate);
            }
        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    private void measureRate(SensorEvent event) {
        if (lastMagUpdate > 0) {
            rate = TimeUnit.NANOSECONDS.toMillis(event.timestamp - lastMagUpdate);
        }
        lastMagUpdate = event.timestamp;
    }

    private void measureIfUpsideDown(float roll) {
        upsideDown = Math.abs(roll) > 100;
    }

    private void measureMagneticInterference(SensorEvent event) {
        double magneticFieldStrength = Math.sqrt((event.values[0] * event.values[0]) + (event.values[1] * event.values[1]) + (event.values[2] * event.values[2]));
        magneticInterference = magneticFieldStrength < SensorManager.MAGNETIC_FIELD_EARTH_MIN || magneticFieldStrength > SensorManager.MAGNETIC_FIELD_EARTH_MAX;
    }


    /**
     * @see 'http://en.wikipedia.org/wiki/Low-pass_filter#Algorithmic_implementation'
     * @see 'http://developer.android.com/reference/android/hardware/SensorEvent.html'
     * #values
     */
    protected float[] lowPassFilter(float[] last, float[] input, long rate) {
        if (last == null) return input;

        final float time = rate * 0.001f; //to sec
        // alpha is calculated as t / (t + dT)
        // with t, the low-pass filter's time-constant
        // and dT, the event delivery rate
        final float alpha = time / (time + 0.4f);
        float[] output = new float[input.length];
        for (int i = 0; i < last.length; i++) {
            output[i] = last[i] + alpha * (input[i] - last[i]);
        }
        return output;
    }

    protected float[] lowPassFilterGoogle(float[] last, float[] input) {
        if (last == null) return input;

        float[] output = new float[input.length];
        for (int i = 0; i < input.length; i++) {
            output[i] = last[i] * (1.0f - ALPHA) + (input[i] * ALPHA);
        }
        return output;
    }

    long lastUpdate = 0;

    //http://developer.android.com/reference/android/hardware/SensorEvent.html
    protected float[] highPassFilter(float[] input, float[] output) {

        lastUpdate = System.currentTimeMillis();
        if (input == null) return output;
        // alpha is calculated as t / (t + dT)
        // with t, the low-pass filter's time-constant
        // and dT, the event delivery rate,  alpha = 0.8f;
        final long rate = System.currentTimeMillis() - lastUpdate;

        final float alpha = 0.5f + rate;
        Log.d("compass", "alfa " + alpha);
        for (int i = 0; i < input.length; i++) {
            output[i] = alpha * input[0] + (1 - alpha) * output[0];
        }

        return output;
    }

    public boolean isUpsideDown() {
        return upsideDown;
    }

    public float getHeading() {
        return heading;
    }

    public float getHeadingToTarget() {
        return (heading - targetHeading + 360) % 360;
    }

    public float getTargetHeading() {
        return targetHeading;
    }

    public void setTargetHeading(float targetHeading) {
        this.targetHeading = targetHeading;
    }

    public boolean hasHeading() {
        return heading < 0;
    }

    public void setHeading(float heading) {
        this.heading = heading;
    }

    public float getPitch() {
        return pitch;
    }

    public float getRoll() {
        return roll;
    }

    public boolean isKeeptLevel() {
        return Math.abs(pitch) < 25 && (Math.abs(roll) < 25 || Math.abs(roll) > 155);
    }

    public boolean hasMagneticInterference() {
        return magneticInterference;
    }


    public boolean needCalibration() {
        return needAccCalibration || needMagCalibration;
    }
}
