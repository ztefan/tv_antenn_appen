package se.softstuff.min_tv_mast.ui.view;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


public class MarkerWithStations extends SimpleMarker {


    private List<MapStation> closestStations = new ArrayList<MapStation>();
    private final List<Polyline> lines;

    MarkerWithStations(final Marker marker, final int accuracy, List<Polyline> lines) {
        super(marker, accuracy);
        this.lines = lines;
    }

    public void setClosestStations(List<MapStation> newStations, boolean isVisibleOnScreen) {
        closestStations.clear();
        closestStations.addAll(newStations);

        Collections.sort(this.closestStations, MapStation.COMPARE_BY_SIZE_AND_DISTANCE);

        for(int i=0; i<lines.size(); i++){
            if(isVisibleOnScreen && i < newStations.size() ){
                final LatLng target = newStations.get(i).getStation().getPosition();
                lines.get(i).setPoints(Arrays.asList(getPosition(), target));
            } else {
                lines.get(i).setPoints(Collections.EMPTY_LIST);
            }
        }
    }

    public List<MapStation> getClosestStations() {
        return closestStations;
    }

    public List<StationEdge> getClosestStationEntities() {
        return FluentIterable.from(closestStations).transform(new Function<MapStation, StationEdge>() {
            @Override
            public StationEdge apply(MapStation input) {
                return input.getStationEdge();
            }
        }).toList();
    }

    public void updateStationRelation(Collection<MapStation> stations, boolean isVisibleOnScreen){

        float[] results = new float[2];
        for (MapStation station : stations) {
            distanceBetween(station.getStation(), results);
            station.setDistance((int) results[0]);
            station.setBearing((int) results[1]);
        }

        List<MapStation> sorted = new ArrayList<MapStation>(stations);
        Collections.sort(sorted, MapStation.COMPARE_BY_DISTANCE);
        final List<MapStation> closestStations = sorted.subList(0, Math.min(lines.size(), sorted.size()));
        boolean foundLargeStation = false;
        for(Iterator<MapStation> it = closestStations.iterator(); it.hasNext();){
            final MapStation mapStation = it.next();
            if(foundLargeStation){
                it.remove();
            } else {
                foundLargeStation = mapStation.isLarge();
            }
        }
        setClosestStations(closestStations, isVisibleOnScreen);

        if(isInfoWindowShown()){
            // update content in info window
            showInfoWindow();
        }

    }

    public void removeMarker() {
        super.removeMarker();
        for(final Polyline line : lines){
            line.remove();
        }
        lines.clear();
    }
}
