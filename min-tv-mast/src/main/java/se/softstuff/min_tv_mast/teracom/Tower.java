package se.softstuff.min_tv_mast.teracom;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Stefan on 2013-11-30.
 */
public class Tower implements Parcelable {
    private String stationID;
    private String stationName;
    private int signalStrength;
    private int compassHeading;
    private int distanceInMetres;

    public Tower(String stationID, String stationName, int signalStrength, int compassHeading, int distanceInMetres) {
        this.stationID = stationID;
        this.stationName = stationName;
        this.signalStrength = signalStrength;
        this.compassHeading = compassHeading;
        this.distanceInMetres = distanceInMetres;
    }

    public Tower(Parcel in) {
        stationID = in.readString();
        stationName = in.readString();
        signalStrength = in.readInt();
        compassHeading = in.readInt();
        distanceInMetres = in.readInt();
    }

    public String getStationID() {
        return stationID;
    }

    public String getStationName() {
        return stationName;
    }

    public int getSignalStrength() {
        return signalStrength;
    }

    public int getCompassHeading() {
        return compassHeading;
    }

    public int getDistanceInMetres() {
        return distanceInMetres;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(stationID);
        out.writeString(stationName);
        out.writeInt(signalStrength);
        out.writeInt(compassHeading);
        out.writeInt(distanceInMetres);
    }

    @Override
    public String toString() {
        String signal = signalStrength > 0 ? ", " + signalStrength + "%" : "";
        return String.format("%s, %s km%s", stationName, distanceInMetres / 1000, signal);
    }

    public static final Parcelable.Creator<Tower> CREATOR
            = new Parcelable.Creator<Tower>() {
        public Tower createFromParcel(Parcel in) {
            return new Tower(in);
        }

        public Tower[] newArray(int size) {
            return new Tower[size];
        }
    };
}
