package se.softstuff.min_tv_mast.teracom;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Represents one station
 *
 * Json:
 *  {"ID":"ARA","Name":"Aareavaara","IsLarge":false,"Latitude":67.452475722151,"Longitude":23.52640128890240,
 "IsComplementary":true,"IsHDReady":false,"HasAlarm":false,"HasNetMessage":true}
 */
public class Station implements Parcelable, ClusterItem {
    public static final String ID = "ID";
    public static final String NAME = "Name";
    public static final String LATITUDE = "Latitude";
    public static final String LONGITUDE = "Longitude";
    public static final String IS_LARGE = "IsLarge";
    public static final String IS_COMPLEMENTARY = "IsComplementary";
    public static final String IS_HDREADY = "IsHDReady";
    public static final String HAS_ALARM = "HasAlarm";
    public static final String HAS_NET_MESSAGE = "HasNetMessage";

    private String id;
    private String name;
    private double latitude;
    private double longitude;
    private boolean large;
    private boolean complementary;
    private boolean hdReady;
    private boolean alarm;
    private boolean netMessage;

    private LatLng lazyLocation;

    public Station() {
    }

    public Station(Parcel parcel) {
        id = parcel.readString();
        name = parcel.readString();
        latitude = parcel.readDouble();
        longitude = parcel.readDouble();
        large = parcel.readByte() != 0;
        complementary = parcel.readByte() != 0;
        hdReady = parcel.readByte() != 0;
        alarm = parcel.readByte() != 0;
        netMessage = parcel.readByte() != 0;
    }

    public Station(JSONObject json) throws JSONException {
        id = json.getString(ID);
        name = json.getString(NAME);
        latitude = json.getDouble(LATITUDE);
        longitude = json.getDouble(LONGITUDE);
        large = json.getBoolean(IS_LARGE);
        complementary = json.getBoolean(IS_COMPLEMENTARY);
        hdReady = json.getBoolean(IS_HDREADY);
        alarm = json.getBoolean(HAS_ALARM);
        netMessage = json.getBoolean(HAS_NET_MESSAGE);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isLarge() {
        return large;
    }

    public void setLarge(boolean large) {
        this.large = large;
    }

    public boolean isComplementary() {
        return complementary;
    }

    public void setComplementary(boolean complementary) {
        this.complementary = complementary;
    }

    public boolean isHdReady() {
        return hdReady;
    }

    public void setHdReady(boolean hdReady) {
        this.hdReady = hdReady;
    }

    public boolean hasAlarm() {
        return alarm;
    }

    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
    }

    public boolean hasNetMessage() {
        return netMessage;
    }

    public void setNetMessage(boolean netMessage) {
        this.netMessage = netMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || ((Object) this).getClass() != o.getClass()) return false;

        Station station = (Station) o;

        if (!id.equals(station.id)) return false;

        return true;
    }

    @Override
    public String toString() {
        return com.google.common.base.Objects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("latitude", latitude)
                .add("longitude", longitude)
                .add("large", large)
                .add("complementary", complementary)
                .toString();
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(id);
        out.writeString(name);
        out.writeDouble(latitude);
        out.writeDouble(longitude);
        out.writeByte((byte) (large ? 1 : 0));
        out.writeByte((byte) (complementary ? 1 : 0));
        out.writeByte((byte) (hdReady ? 1 : 0));
        out.writeByte((byte) (alarm ? 1 : 0));
        out.writeByte((byte) (netMessage ? 1 : 0));

    }

    public static final Parcelable.Creator<Station> CREATOR
            = new Parcelable.Creator<Station>() {
        public Station createFromParcel(Parcel in) {
            return new Station(in);
        }

        public Station[] newArray(int size) {
            return new Station[size];
        }
    };

    @Override
    public LatLng getPosition() {
        if(lazyLocation==null){
            lazyLocation = new LatLng(latitude,longitude);
        }
        return lazyLocation;
    }
}
