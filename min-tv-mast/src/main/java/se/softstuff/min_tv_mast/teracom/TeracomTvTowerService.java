package se.softstuff.min_tv_mast.teracom;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import se.softstuff.min_tv_mast.storage.RestClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan on 2013-11-30.
 */
public class TeracomTvTowerService {

    private static final String LOG_TAG = "TowerService";


    public interface TowerServiceCallback {
        void onTowerFound(List<Tower> towers);
    }

    private TowerServiceCallback serviceCallback;
    private EasyTracker easyTracker;

    public void setServiceCallback(TowerServiceCallback serviceCallback) {
        this.serviceCallback = serviceCallback;
    }

    public void useTracker(EasyTracker easyTracker) {
        this.easyTracker = easyTracker;
    }

    public void fetchTowers(final Location location) {
        AsyncTask<Void, Void, List<Tower>> updateTowers = new AsyncTask<Void, Void, List<Tower>>() {

            @Override
            protected List<Tower> doInBackground(Void... voids) {

                long start = System.currentTimeMillis();

                String link = String.format("http://www.teracom.se/ajaxProxies/bestTransmitter.ashx?lat=%.4f&lng=%.4f",
                        location.getLatitude(), location.getLongitude());

                    RestClient ws = new RestClient();
                final String response;
                try {
                    response = ws.request(link);
                    List<Tower> towers = parseTowerJson(response);

                    long loadTime = System.currentTimeMillis() - start;
                    trackTime("Load towers", loadTime);
                    return towers;
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }


            @Override
            protected void onPostExecute(List<Tower> fetched) {
                serviceCallback.onTowerFound(fetched);
            }

        };
        updateTowers.execute();
//        updateTowers.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR); // level 11
    }


    public List<Station> getAllTowers() throws IOException, JSONException {
        RestClient ws = new RestClient();
        String body = ws.request("http://www.teracom.se/ajaxProxies/stations.ashx");
        return parseStationsFromJson(body);
    }

    private List<Station> parseStationsFromJson(String body) throws JSONException {
        JSONArray stationArr = new JSONArray(body);
        List<Station> stations = new ArrayList<Station>(stationArr.length());
        for (int i = 0; i < stationArr.length(); i++) {
            JSONObject json = (JSONObject) stationArr.get(i);
            stations.add(new Station(json));
        }
        return stations;
    }


    List<Tower> parseTowerJson(String json) {
        List<Tower> towers = new ArrayList<Tower>();

        try {
            JSONArray stations = new JSONArray(json);
            for (int i = 0; i < stations.length(); i++) {
                JSONObject jsonTower = (JSONObject) stations.get(i);
                Tower tower = new Tower(
                        jsonTower.getString("StationID"),
                        jsonTower.getString("StationName"),
                        jsonTower.getInt("SignalStrength"),
                        jsonTower.getInt("CompassHeading"),
                        jsonTower.getInt("DistanceInMetres"));
                towers.add(tower);
            }
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        return towers;
    }

    private void trackTime(String event, long timeMs) {
        if (isEasyTrackerEnabled()) {
            Log.d(LOG_TAG, String.format("feed google analytic that %s took %s ms", event, timeMs));
            easyTracker.send(MapBuilder
                    .createTiming("resources",    // Timing category (required)
                            timeMs,       // Timing interval in milliseconds (required)
                            event,  // Timing name
                            null)           // Timing label
                    .build());
        } else {
            Log.d(LOG_TAG, String.format("do not feed google analytic that %s took %s ms", event, timeMs));
        }
    }

    private boolean isEasyTrackerEnabled() {
        return easyTracker != null;
    }

}
