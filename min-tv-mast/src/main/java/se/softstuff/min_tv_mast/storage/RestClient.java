package se.softstuff.min_tv_mast.storage;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

/**
 * Created by Stefan on 2013-12-20.
 */
public class RestClient {

    private String mockResponse;
    private long downloadedSize;

    public void setMockResponse(String mockResponse) {
        this.mockResponse = mockResponse;
    }

    public String request(String query) throws IOException {

        if(mockResponse != null){
            return mockResponse;
        }

        DefaultHttpClient httpClient = new DefaultHttpClient();

        HttpGet httpGet = new HttpGet(query);
        httpGet.addHeader(HTTP.CONTENT_TYPE, "application/json");
        httpGet.addHeader(HTTP.CONTENT_ENCODING, "utf-8");
        httpGet.addHeader("Accept-Encoding", "gzip");

        HttpResponse httpResponse = httpClient.execute(httpGet);
        HttpEntity httpEntity = httpResponse.getEntity();
        downloadedSize += httpEntity.getContentLength();

        InputStream instream = httpResponse.getEntity().getContent();
        Header contentEncoding = httpResponse.getFirstHeader("Content-Encoding");
        if (contentEncoding != null && contentEncoding.getValue().equalsIgnoreCase("gzip")) {
            instream = new GZIPInputStream(instream);
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(instream, "utf-8"), 8);

        StringBuilder body = new StringBuilder();

        String inputLine;
        while ((inputLine = reader.readLine()) != null) {
            body.append(inputLine);
        }
        instream.close();

        return body.toString();

    }

    public long getDownloadedSize() {
        return downloadedSize;
    }
}
