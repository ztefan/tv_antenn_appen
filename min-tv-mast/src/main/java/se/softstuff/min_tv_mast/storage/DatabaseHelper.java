package se.softstuff.min_tv_mast.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import se.softstuff.min_tv_mast.R;
import se.softstuff.min_tv_mast.teracom.Station;

import java.io.*;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "min-tv-mast";

    // Table Names
    public static final String TABLE_STATION = "station";


    // Table Create Statements
    private static final String CREATE_TABLE_STATION = "CREATE TABLE "+ TABLE_STATION +
            " (" + Station.ID +" TEXT PRIMARY KEY, "+ Station.NAME + " TEXT, " +
            Station.LATITUDE+" REAL, " + Station.LONGITUDE + " REAL, "+
            Station.IS_LARGE+" INTEGER, " + Station.IS_COMPLEMENTARY + " INTEGER, "+Station.IS_HDREADY+" INTEGER, "+
            Station.HAS_ALARM+" INTEGER, " + Station.HAS_NET_MESSAGE + " INTEGER )";

    private static DatabaseHelper INSTANCE = null;

    private final Context context;

    public static synchronized DatabaseHelper getInstance(Context context){
        if(INSTANCE == null){
            INSTANCE = new DatabaseHelper(context);
        }
        return INSTANCE;
    }

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // creating required tables
        db.execSQL(CREATE_TABLE_STATION);

        runScriptFile(R.raw.stations, db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATION);


        // create new tables
        onCreate(db);
    }

    private void runScriptFile(int fileRef, SQLiteDatabase db) {
        final InputStream inputStream = context.getResources().openRawResource(fileRef);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        try {
            while( (line = reader.readLine()) != null){
                db.execSQL(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
