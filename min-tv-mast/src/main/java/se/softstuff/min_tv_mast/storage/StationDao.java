package se.softstuff.min_tv_mast.storage;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import se.softstuff.min_tv_mast.teracom.Station;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Stefan on 2014-02-25.
 */
public class StationDao {


    private static final String TAG = "StationDao";
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;
    private Context context;
    private String[] stationProjection = new String[]{Station.ID, Station.NAME, Station.LATITUDE, Station.LONGITUDE,
            Station.IS_LARGE, Station.IS_COMPLEMENTARY, Station.IS_HDREADY, Station.HAS_ALARM, Station.HAS_NET_MESSAGE};


    public StationDao(Context context){
        this.context = context;
        this.dbHelper = DatabaseHelper.getInstance(context.getApplicationContext());
    }


    public void openReadable() throws SQLException {
        if(db != null){
            throw new IllegalStateException("Already has a database open");
        }
        db = dbHelper.getReadableDatabase();
        Log.d(TAG, "openReadable database");
    }
    public void openWritable() throws SQLException {
        if(db != null){
            throw new IllegalStateException("Already has a database open");
        }
        db = dbHelper.getWritableDatabase();
        // Enable foreign key constraints
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys = ON;");
        }
        Log.d(TAG, "openReadable database");
    }

    public void close() {
        db.close();
        db = null;
        Log.d(TAG, "close database");
    }

    public void shutdown(){
        dbHelper.close();
        Log.d(TAG, "close database helper");
    }

    public void beginTransaction() {
        db.beginTransaction();
        Log.d(TAG, "begin transaction");
    }

    public void setTransactionSuccessful(){
        Log.d(TAG, "begin commit transaction");
        db.setTransactionSuccessful();
        Log.d(TAG, "commit transaction");
    }
    public void endTransaction(){
        if(db.inTransaction()){
            Log.d(TAG, "begin end transaction");
            db.endTransaction();
            Log.d(TAG, "end transaction");
        }
    }


    public List<Station> getAllLargeStation() {

        openReadable();
        try{
            final Cursor cursor = db.query(DatabaseHelper.TABLE_STATION, stationProjection, Station.IS_LARGE + "=?", new String[]{"1"}, null, null, null);

            return loadStations(cursor);
        } finally {
            close();
        }
    }



    public List<Station> getAllStationIn(LatLngBounds bounds) {
        openReadable();
        try{
            StringBuilder selection = new StringBuilder();
            selection.append(Station.LATITUDE).append(" >= ").append(bounds.southwest.latitude).append(" AND ");
            selection.append(Station.LATITUDE).append(" <= ").append(bounds.northeast.latitude).append(" AND ");
            selection.append(Station.LONGITUDE).append(" <= ").append(bounds.southwest.longitude).append(" AND ");
            selection.append(Station.LONGITUDE).append(" >= ").append(bounds.northeast.longitude);
            final Cursor cursor = db.query(DatabaseHelper.TABLE_STATION, stationProjection, selection.toString(), null, null, null, null);

            return loadStations(cursor);
        } finally {
            close();
        }
    }

    public List<Station> getAllStation() {

        openReadable();
        try{
            final Cursor cursor = db.query(DatabaseHelper.TABLE_STATION, stationProjection, null, null, null, null, null);

            return loadStations(cursor);
        } finally {
            close();
        }
    }


    public void syncStations(RestClient ws) throws IOException, JSONException {

        openWritable();

        beginTransaction();

//        File prop = new File(context.getExternalCacheDir(),"stations.sql");
//        prop.delete();
//        Log.d(TAG, "FIL "+prop.getAbsolutePath());
//        PrintWriter out = new PrintWriter(prop, "UTF-8");
        try {

            db.delete(DatabaseHelper.TABLE_STATION, null, null);


            String sql = String.format("INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s,%s,%s) VALUES (?,?,?,?,?,?,?,?,?);",
                    DatabaseHelper.TABLE_STATION, Station.ID, Station.NAME, Station.LATITUDE, Station.LONGITUDE,
                    Station.IS_LARGE, Station.IS_COMPLEMENTARY, Station.IS_HDREADY, Station.HAS_ALARM, Station.HAS_NET_MESSAGE);

            SQLiteStatement statement = db.compileStatement(sql);

            final String response = ws.request("http://www.teracom.se/ajaxProxies/stations.ashx");
            JSONArray stationsJson = new JSONArray(response);
            int col;
            for(int i = 0; i < stationsJson.length(); i++){
                JSONObject stationJson = stationsJson.getJSONObject(i);
                Station station = new Station(stationJson);
                col = 1;
                statement.clearBindings();
                statement.bindString(col++, station.getId());
                statement.bindString(col++, station.getName());
                statement.bindDouble(col++, station.getLatitude());
                statement.bindDouble(col++, station.getLongitude());
                statement.bindLong(col++, asBoolean(station.isLarge()));
                statement.bindLong(col++, asBoolean(station.isComplementary()));
                statement.bindLong(col++, asBoolean(station.isHdReady()));
                statement.bindLong(col++, asBoolean(station.hasAlarm()));
                statement.bindLong(col++, asBoolean(station.hasNetMessage()));
                statement.execute();


//                String insert = String.format("INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s,%s,%s) VALUES ('%s','%s',%s,%s,%s,%s,%s,%s,%s);",
//                        DatabaseHelper.TABLE_STATION, Station.ID, Station.NAME, Station.LATITUDE, Station.LONGITUDE,
//                        Station.IS_LARGE, Station.IS_COMPLEMENTARY, Station.IS_HDREADY, Station.HAS_ALARM, Station.HAS_NET_MESSAGE,
//                        station.getId(), station.getName(), station.getLatitude(), station.getLongitude(),
//                        asBoolean(station.isLarge()), asBoolean(station.isComplementary()), asBoolean(station.isHdReady()),
//                        asBoolean(station.hasAlarm()), asBoolean(station.hasNetMessage()) );
//
//                out.println(insert);
            }

            setTransactionSuccessful();
        } finally {
            endTransaction();
//            out.flush();
//            out.close();
        }

    }

    private long asBoolean(boolean value){
        return value ? 1 : 0;
    }

    private List<Station> loadStations(Cursor cursor) {
        List<Station> stations = new LinkedList<Station>();
        int col;
        if (cursor.moveToFirst()) {
            do {
                col = 0;
                Station station = new Station();
                station.setId(cursor.getString(col++));
                station.setName(cursor.getString(col++));
                station.setLatitude(cursor.getDouble(col++));
                station.setLongitude(cursor.getDouble(col++));
                station.setLarge(cursor.getInt(col++) == 1);
                station.setComplementary(cursor.getInt(col++) == 1);
                station.setHdReady(cursor.getInt(col++) == 1);
                station.setAlarm(cursor.getInt(col++) == 1);
                station.setNetMessage(cursor.getInt(col++) == 1);
                stations.add(station);
            } while (cursor.moveToNext());
        }
        return stations;
    }


    public void deleteAllStations() {
        db.delete(DatabaseHelper.TABLE_STATION, null, null);
    }

    public void insert(Collection<Station> stations){
        String sql = String.format("INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s,%s,%s) VALUES (?,?,?,?,?,?,?,?,?);",
                DatabaseHelper.TABLE_STATION, Station.ID, Station.NAME, Station.LATITUDE, Station.LONGITUDE,
                Station.IS_LARGE, Station.IS_COMPLEMENTARY, Station.IS_HDREADY, Station.HAS_ALARM, Station.HAS_NET_MESSAGE);

        SQLiteStatement statement = db.compileStatement(sql);

        int col;
        for(Station station : stations){
            statement.clearBindings();
            col = 1;
            statement.bindString(col++, station.getId());
            statement.bindString(col++, station.getName());
            statement.bindDouble(col++, station.getLatitude());
            statement.bindDouble(col++, station.getLongitude());
            statement.bindLong(col++, asBoolean(station.isLarge()));
            statement.bindLong(col++, asBoolean(station.isComplementary()));
            statement.bindLong(col++, asBoolean(station.isHdReady()));
            statement.bindLong(col++, asBoolean(station.hasAlarm()));
            statement.bindLong(col++, asBoolean(station.hasNetMessage()));
            statement.execute();
        }
    }
}
