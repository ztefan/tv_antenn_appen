package se.softstuff.min_tv_mast.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

import se.softstuff.min_tv_mast.storage.StationDao;
import se.softstuff.min_tv_mast.teracom.Station;
import se.softstuff.min_tv_mast.teracom.TeracomTvTowerService;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class TeracomUpdateService extends IntentService {
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_SYNC = "se.softstuff.min_tv_mast.action.SYNC";

    private Handler handler = new Handler();

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionSyncronizeDatabase(Context context) {
        Intent intent = new Intent(context, TeracomUpdateService.class);
        intent.setAction(ACTION_SYNC);
        context.startService(intent);
    }

    public TeracomUpdateService() {
        super("TeracomUpdateService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SYNC.equals(action)) {
                handleActionSync();
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionSync() {
        feedback("Sync database");

        List<Station> stations = null;
        try{
            feedback("fetching towers");
            TeracomTvTowerService teracom = new TeracomTvTowerService();
            stations = teracom.getAllTowers();
        } catch (IOException e) {
            e.printStackTrace();
            feedback("synk failed "+e.getMessage());
            return;
        } catch (JSONException e) {
            e.printStackTrace();
            feedback("synk failed "+e.getMessage());
            return;
        }

        StationDao dao = new StationDao(getApplicationContext());
        dao.openWritable();
        dao.beginTransaction();
        try {
            feedback("storing new towers");
            dao.deleteAllStations();
            dao.insert(stations);
            dao.setTransactionSuccessful();
            feedback("synk completed successful");
        } finally {
            dao.endTransaction();
        }

    }


    private void feedback(final String message) {
        handler.post(new Runnable() {
            //      @Override
            public void run() {
                Toast.makeText(getApplicationContext(), message,
                        Toast.LENGTH_SHORT).show();
            }
        });    // Display toast and exit
    }

}
